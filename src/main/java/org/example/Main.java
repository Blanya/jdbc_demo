package org.example;

import org.example.dao.EmployeeDaoImpl;
import org.example.dao.IDaoEmployee;
import org.example.domain.Employee;
import org.example.utils.ConnectionUtils;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class Main {



    public static void main(String[] args) {
        IDaoEmployee iDaoEmployee = new EmployeeDaoImpl();

        // TEST CO
//        try{
//            Connection conn = ConnectionUtils.getMySQLConnection();
//
//            if(conn != null)
//                System.out.println("Connexion ok");
//            else
//                System.out.println("Connexion echoué");
//        } catch (SQLException e)
//        {
//            throw new RuntimeException(e);
//        }

        Employee employee = iDaoEmployee.getEmployee(1);
        System.out.println(employee);

        List<Employee> employeeList = iDaoEmployee.getAllEmployee();
        employeeList.forEach(System.out::println);
    }
}