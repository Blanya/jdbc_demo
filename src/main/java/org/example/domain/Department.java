package org.example.domain;

import java.io.Serializable;

public class Department implements Serializable {
    private Integer depId;
    private String nameDep;

    public Department(){

    }

    public Department(String nameDep) {
        this.nameDep = nameDep;
    }

    public Integer getDepId() {
        return depId;
    }

    public void setDepId(Integer depId) {
        this.depId = depId;
    }

    public String getNameDep() {
        return nameDep;
    }

    public void setNameDep(String nameDep) {
        this.nameDep = nameDep;
    }

    @Override
    public String toString() {
        return "Department{" +
                "depId=" + depId +
                ", nameDep='" + nameDep + '\'' +
                '}';
    }
}
