package org.example.dao;

import com.mysql.cj.jdbc.exceptions.CommunicationsException;
import org.example.domain.Department;
import org.example.domain.Employee;
import org.example.utils.ConnectionUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDaoImpl implements IDaoEmployee{
    @Override
    public Employee getEmployee(Integer id) {
        Employee e = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;

        try {
            connection = ConnectionUtils.getMySQLConnection();
            String sql = "SELECT * FROM employee WHERE id_emp = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);

            rs = statement.executeQuery();

            if(rs.next()){
                e = new Employee(rs.getDate("start_date"), rs.getDate("end_date"), rs.getString("first_name"), rs.getString("last_name"), rs.getString("title"));

                e.setEmpId(rs.getInt("id_emp"));
            }
        } catch (SQLSyntaxErrorException ex) {
            System.err.println("Error : " + ex.getMessage());
        } catch (CommunicationsException exception)
        {
            System.err.println("Error : Connexion non établie");
        } catch (SQLException exception1)
        {
            exception1.printStackTrace();
        }
        finally {
            try {
                if(rs != null)
                    rs.close();
                if(statement != null)
                    statement.close();
                if(connection != null)
                    connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }

        return e;
    }

    @Override
    public List<Employee> getAllEmployee() {
        List<Employee> employees = new ArrayList<>();
        Employee e = null;
        Department d = new Department();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;

        try {
            connection = ConnectionUtils.getMySQLConnection();
            String sql = "SELECT id_emp, first_name, last_name, title, d.id_dep, name_dep FROM employee " +
                    "INNER JOIN department AS d ON d.id_dep = employee.id_dep WHERE d.id_dep = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, 1);
            rs = statement.executeQuery();

            while (rs.next()){
                e = new Employee();
                e.setEmpId(rs.getInt("id_emp"));
                e.setFirstName(rs.getString("first_name"));
                e.setLastName(rs.getString("last_name"));
                e.setTitle(rs.getString("title"));


                d.setDepId(rs.getInt("id_dep"));
                d.setNameDep(rs.getString("name_dep"));

                e.setDepartment(d);

                employees.add(e);
            }
        }catch (SQLSyntaxErrorException ex) {
            System.err.println("Error : " + ex.getMessage());
        } catch (CommunicationsException exception)
        {
            System.err.println("Error : Connexion non établie");
        } catch (SQLException exception1)
        {
            exception1.printStackTrace();
        }
        finally {
            try {
                if(rs != null)
                    rs.close();
                if(statement != null)
                    statement.close();
                if(connection != null)
                    connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        return employees;
    }

    @Override
    public Integer createEmployee(Employee employee) {
        return null;
    }

    @Override
    public void deleteEmployee(Employee employee) {

    }

    @Override
    public void updateEmployee(Employee employee) {

    }
}
