package org.example.dao;

import org.example.domain.Employee;

import java.util.List;

public interface IDaoEmployee {

    Employee getEmployee(Integer id);

    List<Employee> getAllEmployee();

    Integer createEmployee(Employee employee);
    void deleteEmployee(Employee employee);
    void updateEmployee(Employee employee);

}
